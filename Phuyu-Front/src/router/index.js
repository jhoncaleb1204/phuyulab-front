import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login,
      meta: {
        requiresAuth: false,
        requiresVisitor: true
      }
    },
    {
      // para todos
      path: '/HomeGeneral',
      name: 'HomeGeneral',
      component: () => import(/* webpackChunkName: "homeGeneral" */ '../views/HomeGeneral.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      // para profe
      path: '/ProfesorCurso/:id_horario/curso=:id_curso',
      name: 'ProfesorCurso',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/Profesor/ProfesorCurso.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // para profe
      path: '/ProfesorBanco/:id_curso/:id_horario',
      name: 'ProfesorBanco',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/Profesor/ProfesorBanco.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // para jp
      path: '/JPBanco/:id_curso/:id_horario',
      name: 'JPBanco',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/JP/JPBanco.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // para profe
      path: '/ProfesorCurso/:id_lab/fase/:id_fase/horario=:id_horario/curso=:id_curso',
      name: 'ProfesorFaseLive',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/Profesor/ProfesorFaseLive.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // para jp
      path: '/JPCurso/:id_lab/fase/:id_fase/horario=:id_horario/curso=:id_curso',
      name: 'JPFaseLive',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/JP/JPFaseLive.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // para profe
      path: '/ProfesorCurso/:id_lab/fase/:id_fase/horario=:id_horario/Preguntas=:id_alumno',
      name: 'ProfesorLivePreg',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/Profesor/ProfesorComentarPreg.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // para jp
      path: '/JPCurso/:id_lab/fase/:id_fase/horario=:id_horario/Preguntas=:id_alumno',
      name: 'JPLivePreg',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/JP/JPComentarPreg.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // para jp
      path: '/JPCurso/:id_horario/curso=:id_curso',
      name: 'JPCurso',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/JP/JPCurso.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // para profe
      path: '/ProfesorFase',
      name: 'ProfesorFase',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/Profesor/ProfesorFase.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // para jp
      path: '/JPFase',
      name: 'JPFase',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesorCurso" */ '../views/JP/JPFase.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // para profe
      path: '/Profesor',
      name: 'Profesor',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Profesor/ProfesorHome.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // para profe
      path: '/ProfesorCurso/:id_horario/:id_curso/:id_lab/:id_fase/Laboratorio/Fase',
      name: 'ProfesorRevisar',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Profesor/ProfesorRevisarPreg.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // para jp
      path: '/JPCurso/:id_horario/:id_curso/:id_lab/:id_fase/Laboratorio/Fase',
      name: 'JPRevisar',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/JP/JPRevisarPreg.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // para profe
      // path: '/ProfesorCurso/estadistica',
      path: '/estadistica',
      name: 'ProfesorEstadistica',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Profesor/EstadisticasNotasLaboratorios.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // para jp
      // path: '/ProfesorCurso/estadistica',
      path: '/estadisticaJP',
      name: 'JPEstadistica',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/JP/EstadisticasNotasLaboratoriosJP.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // super admin
      /* path: '/infoCurso/:id', */
      path: '/infoCurso',
      name: 'infoCurso',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "infoCurso" */ '../views/InfoCurso.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'super_admin'
      }
    },
    {
      // Admin
      path: '/AdminInicio',
      name: 'AdminInicio',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Administrador/InicioAdmin.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'admin'
      }
    },
    {
      // Admin
      path: '/agregarParticipanteAdmin/:id',
      name: 'AgregarParticipanteAdmin',
      component: () => import(/* webpackChunkName: "profesor" */ '../views/AgregarParticipantesHorario.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'admin'
      }
    },
    {
      // admin
      path: '/horarioxcurso/:id',
      name: 'HorarioxCurso',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Administrador/HorariosxCurso.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'admin'
      }
    },
    {
      // admin
      path: '/gestionarHorario/:id',
      name: 'GestionarHorario',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Administrador/GestionarHorario.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'admin'
      }
    },
    {
      // admin
      path: '/AdminCargarCursos',
      name: 'AdminCargarCursos',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Administrador/CargarCursosAdmin.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'admin'
      }
    },
    {
      // admin
      path: '/AdminCargarHorarios',
      name: 'AdminCargarHorarios',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Administrador/CargarHorariosAdmin.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'admin'
      }
    },
    {
      // alumno
      path: '/Alumno/laboratorioHorario',
      name: 'LaboratorioHorario',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Alumno/AlumnoLaboratorios.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'estudiante'
      }
    },
    {
      // alumno
      path: '/Alumno/Horario/Laboratorio/Fase',
      name: 'LaboratorioFase',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Alumno/AlumnoFase.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'estudiante'
      }
    },
    {
      // alumno
      path: '/Alumno/Horario/Laboratorio/Fase/Revisar',
      name: 'RevisarPreguntasAlumno',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Alumno/RevisarPreguntasAlumno.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'estudiante'
      }
    },
    {
      // profesor
      path: '/ProfesorCurso/:id_horario/:id_curso/asistencia',
      name: 'Asistencia',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/AsistenciaLabs.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // jp
      path: '/JPCurso/:id_horario/:id_curso/asistencia',
      name: 'AsistenciaJP',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/AsistenciaLabsJP.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // profesor
      path: '/ProfesorCurso/:id_horario/:id_curso/notas',
      name: 'Notas',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/NotaLabs.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // jp
      path: '/JPCurso/:id_horario/:id_curso/notas',
      name: 'NotasJP',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/NotasLabsJP.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // profesor
      path: '/ProfesorCurso/:id_horario/:id_curso/participantes',
      name: 'Participantes',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Profesor/ParticipantesCurso.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // jp
      path: '/JPCurso/:id_horario/:id_curso/participantes',
      name: 'ParticipantesJP',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/JP/ParticipantesCursoJP.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // profesor
      path: '/ProfesorCurso/:id_horario/:id_curso/:id_lab/:id_fase',
      name: 'ResumenRespuestas',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Profesor/RevisarPregResumenFase.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // jp
      path: '/JPCurso/:id_horario/:id_curso/:id_lab/:id_fase',
      name: 'ResumenRespuestasJP',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/JP/RevisarPregResumenFaseJP.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // profesor
      path: '/ProfesorCurso/:id_horario/:id_curso/:id_lab',
      name: 'ResumenNotasFase',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Profesor/NotasFases.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // jp
      path: '/JPCurso/:id_horario/:id_curso/:id_lab',
      name: 'ResumenNotasFaseJP',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/JP/NotasFasesJP.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    },
    {
      // profesor
      path: '/ProfesorCurso/:id_horario/:id_curso/participantes/agregar',
      name: 'AgregarParticipantes',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/AgregarParticipantesACurso.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'profesor'
      }
    },
    {
      // superadmin
      path: '/SuperAdmin',
      name: 'SuperAdmin',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/SuperAdminHome.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'super_admin'
      }
    },
    {
      // superadmin
      path: '/SuperAdmin/Facultades',
      name: 'SuperAdmin_Facultades',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/SuperAdmin_Facultades.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'super_admin'
      }
    },
    {
      // superadmin
      path: '/SuperAdmin/Especialidades',
      name: 'SuperAdmin_Especialidades',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/SuperAdmin_Especialidades.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'super_admin'
      }
    },
    {
      // superadmin
      path: '/SuperAdmin/Administradores',
      name: 'SuperAdmin_Administradores',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/SuperAdmin_Administradores.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'super_admin'
      }
    },
    {
      // super admin
      path: '/SuperAdmin/Usuarios',
      name: 'SuperAdmin_Usuarios',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/SuperAdmin_Usuarios.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'super_admin'
      }
    },
    {
      // alumno
      path: '/AlumnoCurso/:id_horario',
      name: 'AlumnoCurso',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Alumno/AlumnoCurso.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'estudiante'
      }
    },
    {
      // alumno
      path: '/alumnoInicio',
      name: 'AlumnoInicio',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Alumno/AlumnoInicio.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'estudiante'
      }
    },
    {
      // alumno
      path: '/Alumno/Actividades',
      name: 'AlumnoActividades',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Alumno/AlumnoActividades.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'estudiante'
      }
    },
    {
      // alumno
      path: '/Alumno/Calendario',
      name: 'AlumnoCalendario',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Alumno/AlumnoCalendario.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'estudiante'
      }
    },
    {
      // alumno
      path: '/AlumnoCurso/:id_horario/participantes',
      name: 'ParticipantesA',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Alumno/ParticipantesCurso.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'estudiante'
      }
    },
    {
      // alumno
      path: '/AlumnoCurso/:id_horario/notas',
      name: 'AlumnoNotas',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/Alumno/AlumnoNotas.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'estudiante'
      }
    },
    {
      // jp
      path: '/JefePractica',
      name: 'JefePractica',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profesor" */ '../views/JP/JPHome.vue'),
      meta: {
        requiresAuth: true,
        requiresPerm: true,
        perm: 'jefe_practica'
      }
    }
  ],
  mode: 'history',
  base: process.env.BASE_URL
})
