// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import vuetify from '@/plugins/vuetify'
import ElementUI from 'element-ui'
import './style/theme/index.css'
import VueAxios from 'vue-axios'
import axios from 'axios'
import store from '@/store/store'
import GAuth from 'vue-google-oauth2'
import * as VeeValidate from 'vee-validate'
import VueParticlesBg from 'particles-bg-vue'
const gauthOption = {
  clientId: '726801571485-ehv9fsh6fpc33hqc0clo3kotl7gubk5o.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
}
Vue.use(GAuth, gauthOption)
Vue.use(ElementUI)
Vue.use(VueAxios, axios)
Vue.use(VeeValidate)
Vue.use(VueParticlesBg)
Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.loggedIn) {
      next({
        name: 'Login'
      })
    } else {
      if (to.matched.some(record => record.meta.requiresPerm)) {
        switch (to.meta.perm) {
          case 'profesor':
            if (!store.getters.hasPermissionProf) {
              next({
                name: 'HomeGeneral'
              })
            } else {
              next()
            }
            break
          case 'estudiante':
            if (!store.getters.hasPermissionEst) {
              next({
                name: 'HomeGeneral'
              })
            } else {
              next()
            }
            break
          case 'admin':
            if (!store.getters.hasPermissionAdmin) {
              next({
                name: 'HomeGeneral'
              })
            } else {
              next()
            }
            break
          case 'super_admin':
            if (!store.getters.hasPermissionSadmin) {
              next({
                name: 'HomeGeneral'
              })
            } else {
              next()
            }
            break
          case 'jefe_practica':
            if (!store.getters.hasPermissionJP) {
              next({
                name: 'HomeGeneral'
              })
            } else {
              next()
            }
            break
          default:
            next()
        }
      } else {
        next()
      }
    }
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    if (store.getters.loggedIn) {
      next({
        name: 'HomeGeneral'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  vuetify,
  components: { App },
  template: '<App/>'
})
