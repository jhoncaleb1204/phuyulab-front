import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
import extraConfig from '../extraConfig'
axios.defaults.baseURL = extraConfig.backPath
Vue.use(Vuex)
export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    img: localStorage.getItem('img') || null,
    token: localStorage.getItem('access_token') || null,
    refreshToken: localStorage.getItem('refresh_token') || null,
    permissions: localStorage.getItem('permissions') || null,
    username: null
  },
  getters: {
    loggedIn (state) {
      return state.token !== null
    },
    hasPermissionProf (state) {
      if (state.permissions) {
        return state.permissions.includes('profesor')
      } else {
        return false
      }
    },
    hasPermissionEst (state) {
      if (state.permissions) {
        return state.permissions.includes('estudiante')
      } else {
        return false
      }
    },
    hasPermissionAdmin (state) {
      if (state.permissions) {
        return state.permissions.includes('admin')
      } else {
        return false
      }
    },
    hasPermissionSadmin (state) {
      if (state.permissions) {
        return state.permissions.includes('super_admin')
      } else {
        return false
      }
    },
    hasPermissionJP (state) {
      if (state.permissions) {
        return state.permissions.includes('jefe_practica')
      } else {
        return false
      }
    }
  },
  mutations: {
    retrieveToken (state, token) {
      state.token = token.access_token
      state.refreshToken = token.refresh_token
      state.username = token.username
      state.permissions = token.permissions
      state.img = token.img
    },
    destroyToken (state, token) {
      state.token = null
      state.refreshToken = null
      state.username = null
      state.permissions = null
      state.img = null
    }
  },
  actions: {
    async retrieveToken (context, data) {
      console.log('loging in')
      console.log(extraConfig)
      try {
        const gUser = await this._vm.$gAuth.signIn()
        const response = await axios.post('/api/google/', {'token': gUser.xc.access_token})
        if (response.status !== 200) {
          throw response.data
        }
        // console.log('guser------')
        // console.log(gUser)
        const token = response.data.access_token
        const refreshToken = response.data.refresh_token
        localStorage.setItem('access_token', token)
        localStorage.setItem('refresh_token', refreshToken)
        localStorage.setItem('permissions', response.data.permissions)
        if (gUser.xt) {
          localStorage.setItem('img', gUser.xt.iK)
          response.data.img = gUser.xt.iK
        } else {
          // console.log('wt ----')
          // console.log(gUser.wt)
          localStorage.setItem('img', gUser.wt.SJ)
          response.data.img = gUser.wt.SJ
        }
        // response.data.username = gUser.xt.Ad
        context.commit('retrieveToken', response.data)
        console.log('logged in :D')
      } catch (error) {
        console.error(error)
      }
    },
    destroyToken (context) {
      if (context.getters.loggedIn) {
        localStorage.removeItem('access_token')
        localStorage.removeItem('refresh_token')
        localStorage.removeItem('permissions')
        localStorage.removeItem('img')
        context.commit('destroyToken')
      }
    }
  }
})
