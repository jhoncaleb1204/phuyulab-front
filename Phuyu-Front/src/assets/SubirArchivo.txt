template
<div class="container">
                  <form @submit.prevent="onSubmit">
                      <div class="form-group">
                          <input type="file" accept="image/*" @change="onFileUpload">
                      </div>
                      <div class="form-group">
                          <input type="text" v-model="name" placeholder="Nombre de archivo" class="form-control">
                      </div>
                      <div class="form-group">
                          <button class="btn btn-primary btn-block btn-lg">SUBIR ARCHIVO</button>
                      </div>
                  </form>
              </div>


script
 methods: {
    onFileUpload (event) {
      this.FILE = event.target.files[0]
    },
    onSubmit () {
      // upload file
      const formData = new FormData()
      formData.append('imagen', this.FILE, this.FILE.name)

      axios.patch('http://ec2-54-92-230-86.compute-1.amazonaws.com/api/institucion/1/',
        formData, {headers: {
          'Content-Type': 'multipart/form-data'}
        }).then((res) => {
        console.log(res)
        location.reload()
      })
    },
















// Creamos el objeto de la clase FileReader
      let reader = new FileReader();
      reader.readAsDataURL(this.file2);
      // Le decimos que cuando este listo ejecute el código interno
      reader.onload = function(){
        let preview = document.getElementById('preview'),
                image = document.createElement('img');
        image.src = reader.result;
        console.log(reader.result);
        
        preview.innerHTML = '';
        preview.append(image);
      };